import { NavLink } from 'react-router-dom';

function Nav() {
    return (
        <>
        <nav className="navbar navbar-expand-xl navbar-light bg-light">
            <div className="container-fluid">
                <NavLink className="navbar-brand" to="/">Boba Boba</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse show" id="navbarBasic">
                <div className="container text-center">
                    <div class="row justify-content-md-center">
                        <div className="col-sm-6">
                        <form className="d-flex">
                    <input className="form-control me-2" type="search" placeholder="Search location" aria-label="Search"/>
                    <button className="btn btn-outline-dark" type="submit">Search</button>
                </form>
                </div>
                    <div className="col-sm-4">
                    <ul className="navbar-nav me-auto mb-2 mb-xxl-0">
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="reviews/new">Write a review</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="bobashops/new">Add new Bobashop</NavLink>
                    </li>
                </ul>
                    </div>
                </div>
                </div>
            </div>
        </div>
        </nav>
        </>
    )
}

export default Nav;
