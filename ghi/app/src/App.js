import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ReviewCreate from './ReviewCreate';
import BobashopCreate from './BobashopCreate';
import BobashopList from './BobashopList';

function App() {
  return (
    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/reviews/new" element={<ReviewCreate />} />
        <Route path="bobashops/new" element={<BobashopCreate />} />
        <Route path="bobashops" element={<BobashopList />} />
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
