import React, { useState } from 'react';
import Rating from '@mui/material/Rating';

function ReviewCreate(props) {
    const [bobashop, setBobashop] = useState('')
    const [rating, setRating] = useState('')
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a review</h1>
            <form className="d-inline-flex">
            <input className="form-control mt-3" type="search" aria-label="Search" placeholder="Bobashop"/>
            <button className="btn btn-outline-dark mt-3">Search</button>
        </form>
            <form>
                <div className="form-floating mb3 p-4 me-1 bd-highlight">
                <Rating
                    name="simple-controlled"
                    value={rating}
                    onChange={(event, newValue) => {
                        setRating(newValue);
                    }}
                />
                </div>
                <div className="form-floating mb-3">
                <input placeholder="Title" type="text" name="title" id="title" className="form-control"></input>
                <label htmlFor="title">Title(optional)</label>
                </div>
                <div className="form-floating mb-3">
                <label>Description(optional)</label>
                <textarea rows="4" cols="50" id="description"></textarea>
                </div>
                <div className="form-floating mb-3">
                <input placeholder="Author" type="text" name="author" id="author" className="form-control"></input>
                <label htmlFor="author">Author(optional)</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )
}

export default ReviewCreate;
