from django.contrib import admin
from django.urls import path
from .views import show_bobashop_list, show_bobashop_details, show_reviews_list, show_review_detail

urlpatterns = [
    path("bobashops/", show_bobashop_list, name="show_bobashop_list"),
    path("bobashops/<int:id>", show_bobashop_details, name="show_bobashop_details"),
    path("<int:id>/reviews", show_reviews_list, name="show_reviews_list"),
]
