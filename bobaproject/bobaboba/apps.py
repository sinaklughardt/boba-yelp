from django.apps import AppConfig


class BobabobaConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "bobaboba"
