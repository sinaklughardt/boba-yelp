from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Bobashop, Review
from common.json import ModelEncoder
import json

class BobashopEncoder(ModelEncoder):
    model = Bobashop
    properties = ["name", "address", "id"]

class ReviewEncoder(ModelEncoder):
    model = Review
    properties = ["rating", "id", "bobashop"]

    encoders = {
        "bobashop": BobashopEncoder()
    }



@require_http_methods(["GET", "POST"])
def show_bobashop_list(request):
    if request.method == "GET":
        bobashops = Bobashop.objects.all()
        return JsonResponse(
            {"bobashops": bobashops},
            encoder=BobashopEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            bobashop = Bobashop.objects.create(**content)
            return JsonResponse(
                bobashop,
                encoder=BobashopEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a bobashop"}
            )
            response.status_code = 400
            return response

require_http_methods("GET")
def show_bobashop_details(request, id):
    if request.method == "GET":
        bobashop = Bobashop.objects.get(id=id)
        return JsonResponse(
            bobashop,
            encoder=BobashopEncoder,
            safe=False
        )

require_http_methods(["GET", "POST"])
def show_reviews_list(request,id):
    if request.method == "GET":
        reviews = Review.objects.filter(bobashop=id)
        return JsonResponse(
            {"reviews": reviews},
            encoder=ReviewEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            bobashop = Bobashop.objects.get(id=id)
            content["bobashop"] = bobashop
            review = Review.objects.create(**content)
            return JsonResponse(
                review,
                encoder=ReviewEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create review"}
            )
            response.status_code = 400
            return response

def show_review_detail():
    pass
