from django.db import models

class Bobashop(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.PositiveSmallIntegerField(null=True)
    profile_picture = models.CharField(max_length=200, default="https://images.unsplash.com/photo-1637273484213-3b41dfbdcf99?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80")
    hours_start = models.TimeField(null=True)
    hours_end = models.TimeField(null=True)
    website = models.URLField(null=True)
    gps = models.CharField(max_length=300, null=True)
    id = models.IntegerField(primary_key=True)

class Review(models.Model):
    rating = models.PositiveSmallIntegerField()
    title = models.CharField(max_length=200)
    description = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    author = models.CharField(max_length=100, null=True)
    bobashop = models.ForeignKey(
        Bobashop,
        related_name="reviews",
        on_delete=models.CASCADE
    )
